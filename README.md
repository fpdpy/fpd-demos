Introduction
============

Notebook demos for the FPD package (https://gitlab.com/fpdpy/fpd).

The gitlab online notebook rendering is imperfect, with missing images. HTML
versions can be accessed at the link(s) below:

- https://fpdpy.gitlab.io/fpd-demos/01_fpd_overview.html
